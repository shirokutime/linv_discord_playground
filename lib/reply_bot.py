import random
import logging

EMOJI_TRIGGER = '@水橙'
EMOJI_LIST = [
    ":sweat_smile:",
    ":smiling_face_with_3_hearts:",
    ":face_vomiting:"
]


async def reply_emoji(message):
    logging.info(f'Replying emoji as message starting with {EMOJI_TRIGGER} is found')
    await message.reply(random.choice(EMOJI_LIST))

async def process_reply(message):
    if message.content.startswith(EMOJI_TRIGGER):
        await reply_emoji(message)