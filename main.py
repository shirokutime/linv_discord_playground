import discord
import os
from dotenv import load_dotenv
import lib.reply_bot as REPLY_BOT
import logging

# setup env
logging.basicConfig(level=logging.INFO)
load_dotenv()
client = discord.Client()

ALLOWED_CHANNEL = [
    "general",
    '堅系學術台'
]

@client.event
async def on_ready():
    # login feedback
    logging.info('We have logged in as {0.user}'.format(client))


@client.event
async def on_message(message):
    if str(message.channel) not in ALLOWED_CHANNEL:
        logging.info('Skipping as channel not allowed: {message.channel}')
        return
    if message.author == client.user:
        logging.info('Skipping process my own message')
        return

    await REPLY_BOT.process_reply(message)



# Get token for login
token = os.getenv('TOKEN')
logging.debug(f"token is {token}")

# start login process
client.run(token)