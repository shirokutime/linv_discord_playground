# Discord Bot Playground

---

## Build and run
To run the bot, run the following command:
1. install requirements
    > $ pip3 install -r requirements.txt
1. Update `.env` file and replace with discord bot token
1. run the main program
    > $ python3 main.py > output.log 2>&1 &
1. view log
    > $ tail -f ./output.log

---

## Folder Structure
```
├── main.py             # logic for login
├── .env                # env token
└── lib
    └── reply_bot.py    # logic for replying
```

---

## Resources
- [Basic walkthrough](https://www.freecodecamp.org/news/create-a-discord-bot-with-python/)
- [Official Discord Doc](https://discord.com/developers/docs/topics/teams)
- [Discord.py Doc](https://discordpy.readthedocs.io/en/stable/api.html)
